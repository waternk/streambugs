History
=======

1.1 (2019-07-12)
------------------

* Added extended example model.
* Added food web plot function.
* Added feeding links count.
* S3 `plot` dispatching works now on `run.streambugs` results.

1.0 (2017-11-07)
------------------

* The model version 1.0 <DOI:10.1890/12-0591.1> with extensions and the core functions
  <DOI:10.1021/acs.est.5b04068>, <DOI:10.1002/eap.1530>, <DOI:10.1111/fwb.12927>.
